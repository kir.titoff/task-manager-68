package ru.t1.ktitov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.Session;
import ru.t1.ktitov.tm.model.User;

import java.util.List;

@Repository
public interface ISessionRepository extends IUserOwnedRepository<Session> {

    @Nullable
    List<Session> findByUser(@NotNull final User user);

    @Nullable
    @Query("SELECT s FROM Session s WHERE s.id = :id AND s.user = :user")
    Session findByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );

    @Query("SELECT COUNT(1) = 1 FROM Session s WHERE s.id = :id AND s.user = :user")
    Boolean existsByIdAndUser(
            @NotNull @Param("user") final User user,
            @NotNull @Param("id") final String id
    );

}
