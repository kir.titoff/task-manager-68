package ru.t1.ktitov.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
public interface IUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends IDtoRepository<M> {
}
