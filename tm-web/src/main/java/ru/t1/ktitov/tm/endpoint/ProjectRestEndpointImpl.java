package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.IProjectRestEndpoint;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ProjectService;

import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public final class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @PostMapping("/create")
    public void create() {
        projectService.create("New Project " + System.currentTimeMillis());
    }

    @Override
    @PutMapping("/save")
    public void save(@RequestBody final Project project) {
        projectService.update(project);
    }

    @Override
    @DeleteMapping("/delete")
    public void delete(@RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @DeleteMapping("/delete/{id}")
    public void deleteById(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @GetMapping("/find/{id}")
    public Project findById(@PathVariable("id") final String id) {
        return projectService.findOneById(id);
    }

}
