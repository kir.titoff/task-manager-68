package ru.t1.ktitov.tm.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.ktitov.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "tm_project")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "project", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "created",
        "updated",
        "dateStart",
        "dateFinish",
        "userId"
})
public final class Project {

    @Id
    @NotNull
    @XmlElement
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    @XmlElement
    private String name;

    @Column
    @Nullable
    @XmlElement
    private String description;

    @Column
    @NotNull
    @XmlElement
    @XmlSchemaType(name = "string")
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Column
    @NotNull
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date created = new Date();

    @Column
    @NotNull
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updated = new Date();

    @Column
    @Nullable
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @Column
    @Nullable
    @XmlElement
    @XmlSchemaType(name = "dateTime")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @NotNull
    @XmlElement
    @Column(name = "user_id")
    private String userId = "71187d89-af5a-4abf-8c32-31dab120a298";

    public Project(@NotNull final String name) {
        this.name = name;
    }

}
