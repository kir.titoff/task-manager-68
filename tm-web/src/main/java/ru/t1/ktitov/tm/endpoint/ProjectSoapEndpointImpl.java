package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.ktitov.tm.dto.soap.*;
import ru.t1.ktitov.tm.service.ProjectService;

@Endpoint
public final class ProjectSoapEndpointImpl {

    public final static String LOCATION_URI = "/ws";

    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    public final static String NAMESPACE = "http://tm.ktitov.t1.ru/dto/soap";

    @Autowired
    private ProjectService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "projectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@RequestPayload final ProjectCreateRequest request) {
        return new ProjectCreateResponse(projectService.create("New Project " + System.currentTimeMillis()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@RequestPayload final ProjectSaveRequest request) {
        projectService.update(request.getProject());
        return new ProjectSaveResponse(true);
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteRequest", namespace = NAMESPACE)
    public ProjectDeleteResponse delete(@RequestPayload final ProjectDeleteRequest request) {
        projectService.remove(request.getProject());
        return new ProjectDeleteResponse(true);
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@RequestPayload final ProjectDeleteByIdRequest request) {
        projectService.removeById(request.getId());
        return new ProjectDeleteByIdResponse(true);
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@RequestPayload final ProjectFindAllRequest request) {
        return new ProjectFindAllResponse(projectService.findAll());
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@RequestPayload final ProjectFindByIdRequest request) {
        return new ProjectFindByIdResponse(projectService.findOneById(request.getId()));
    }

}
