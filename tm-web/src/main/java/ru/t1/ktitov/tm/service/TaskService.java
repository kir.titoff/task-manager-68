package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.ITaskRepository;
import ru.t1.ktitov.tm.model.Task;

import javax.transaction.Transactional;
import java.util.*;

@Service
public class TaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Transactional
    public Task add(@Nullable final Task model) {
        repository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Transactional
    public Collection<Task> add(@Nullable final Collection<Task> models) {
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Transactional
    public Task update(@Nullable final Task model) {
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    public List<Task> findAll() {
        return repository.findAll();
    }

    @Nullable
    public Task findOneById(@Nullable final String id) {
        return repository.findById(id).orElse(null);
    }

    @NotNull
    @Transactional
    public Task remove(@Nullable final Task model) {
        repository.delete(model);
        return model;
    }

    @NotNull
    @Transactional
    public Task removeById(@Nullable final String id) {
        Optional<Task> model = repository.findById(id);
        repository.deleteById(id);
        return model.get();
    }

    public Task create(
            @Nullable final String name
    ) {
        @NotNull final Task project = new Task();
        project.setName(name);
        add(project);
        return project;
    }

}
