package ru.t1.ktitov.tm.dto.soap;

import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
@NoArgsConstructor
public final class ObjectFactory {

    public ProjectCreateRequest createProjectRequest() {
        return new ProjectCreateRequest();
    }

    public ProjectCreateResponse createProjectResponse() {
        return new ProjectCreateResponse();
    }

    public ProjectSaveRequest saveProjectRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse saveProjectResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectDeleteRequest deleteProjectRequest() {
        return new ProjectDeleteRequest();
    }

    public ProjectDeleteResponse deleteProjectResponse() {
        return new ProjectDeleteResponse();
    }

    public ProjectDeleteByIdRequest deleteProjectByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse deleteProjectByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectFindAllRequest findAllProjectsRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse findAllProjectsResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectFindByIdRequest findProjectByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse findProjectByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public TaskCreateRequest createTaskRequest() {
        return new TaskCreateRequest();
    }

    public TaskCreateResponse createTaskResponse() {
        return new TaskCreateResponse();
    }

    public TaskSaveRequest saveTaskRequest() {
        return new TaskSaveRequest();
    }

    public TaskSaveResponse saveTaskResponse() {
        return new TaskSaveResponse();
    }

    public TaskDeleteRequest deleteTaskRequest() {
        return new TaskDeleteRequest();
    }

    public TaskDeleteResponse deleteTaskResponse() {
        return new TaskDeleteResponse();
    }

    public TaskDeleteByIdRequest deleteTaskByIdRequest() {
        return new TaskDeleteByIdRequest();
    }

    public TaskDeleteByIdResponse deleteTaskByIdResponse() {
        return new TaskDeleteByIdResponse();
    }

    public TaskFindAllRequest findAllTasksRequest() {
        return new TaskFindAllRequest();
    }

    public TaskFindAllResponse findAllTasksResponse() {
        return new TaskFindAllResponse();
    }

    public TaskFindByIdRequest findTaskByIdRequest() {
        return new TaskFindByIdRequest();
    }

    public TaskFindByIdResponse findTaskByIdResponse() {
        return new TaskFindByIdResponse();
    }

}
