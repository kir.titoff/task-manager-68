package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataJsonJaxBLoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataJsonJaxBLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-json-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from json file by jaxb";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJsonJaxBLoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataJsonJaxBLoadRequest request = new DataJsonJaxBLoadRequest(getToken());
        domainEndpoint.loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
