package ru.t1.ktitov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.ktitov.tm.dto.request.data.DataXmlJaxBLoadRequest;
import ru.t1.ktitov.tm.event.ConsoleEvent;

@Component
public class DataXmlJaxBLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    public static final String DESCRIPTION = "Load data from xml file by jaxb";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlJaxBLoadListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull DataXmlJaxBLoadRequest request = new DataXmlJaxBLoadRequest(getToken());
        domainEndpoint.loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
